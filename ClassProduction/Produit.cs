﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassProduction {
    
    public class Produit {
        
        private int reference;
        private string libelle;
        private double coutProduction;
        private double coefficientMarge;
        private int stock;
        private int seuilMini;
        private int seuilMaxi;
        private List<int> lesMesures;

        /// <summary>
        /// Constructeur: inititalise les attributs avec les valeurs passées en paramètre,
        /// le stock à 0, la liste des mesures à une liste vide
        /// </summary>
        public Produit(int uneReference, string unLibelle, double unCoutProd, double unCoeffMarge, int unSeuilMini,
            int unSeuilMaxi) {
            this.reference = uneReference;
            this.libelle = unLibelle;
            this.coutProduction = unCoutProd;
            this.coefficientMarge = unCoeffMarge;
            this.stock = 0;
            this.seuilMini = unSeuilMini;
            this.seuilMaxi = unSeuilMaxi;
            this.lesMesures = new List<int>();
        }

        /// <summary>
        /// Méthode qui calcule et retourne le prix de vente du produit
        /// </summary>
        /// <returns>prix de vente</returns>
        public double CalculerPrixVente() {
            double prixVente;
            prixVente = this.coutProduction * this.coefficientMarge;
            return prixVente;
        }

        /// <summary>
        /// Méthode qui indique si le produit est disponible ou non
        /// </summary>
        /// <returns>true si le produit est disponible, false si le produit est indisponible</returns>
        public bool EtreDisponible() {
            bool dispo = true;
            if (this.stock == 0)
                dispo = false;
            return dispo;
        }

        /// <summary>
        /// Méthode qui met à jour le stock (stock augmenté de la quantité fabriquée)
        /// </summary>
        /// <param name="uneQteFabriquee">quantité fabriquée du produit</param>
        public void Fabriquer(int uneQteFabriquee) {
            this.stock = this.stock + uneQteFabriquee;
        }

        /// <summary>
        /// Méthode qui met à jour le stock (stock diminué de la quantité vendue) si la vente est possible (stock suffisant)
        /// </summary>
        /// <param name="uneQteVendue">quantité vendue du produit</param>
        /// <returns>true si la vente est possible, false sinon</returns>
        public bool Vendre(int uneQteVendue) {
            bool okVente = true;
            if (this.stock < uneQteVendue)
                okVente = false;
            else
                this.stock = this.stock - uneQteVendue;
            return okVente;
        }

        /// <summary>
        /// Méthode qui retourne la description du produit sous la forme d'une chaîne
        /// </summary>
        /// <returns></returns>
        public string ObtenirDescription() {
            string valRetour = "DESCRIPTIF PRODUIT : \n";
            valRetour += this.reference + " - " + this.libelle;
            valRetour += "\nPrix de vente : " + this.CalculerPrixVente() + " euros";
            valRetour += "\nIntervalle tolérance : [" + this.seuilMini + " - " + this.seuilMaxi + "]";
            return valRetour;
        }

        /// <summary>
        /// Permet d'ajouter à la liste lesMesures une mesure qualité effectuée
        /// </summary>
        /// <param name="uneMesure">valeur relevée de type int</param>
        public void AjouterMesureQualite(int uneMesure) {
            this.lesMesures.Add(uneMesure);
        }

        /// <summary>
        /// Méthode qui permet de calculer la moyenne des mesures qualité relevées
        /// </summary>
        /// <returns>Valeur moyenne</returns>
        public double CalculerMesureMoyenne() {
            double somme = 0;
            foreach (int m in this.lesMesures) {
                somme += m;
            }

            return somme / this.lesMesures.Count;
        }

        /// <summary>
        /// Méthode qui retourne le nombre de défauts qualité (valeur relevée hors intervalle de tolérance)
        /// parmi les mesures effectuées
        /// </summary>
        /// <returns>Nombre de défauts qualité</returns>
        public int CalculerNb() {
            int nb = 0;
            foreach (int m in this.lesMesures) {
                if (m < this.seuilMini || m > this.seuilMaxi)
                    nb++;
            }

            return nb;
        }
    }
}